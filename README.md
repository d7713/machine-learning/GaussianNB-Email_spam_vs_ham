# Email spam vs ham (Gaussian Naive Bayes)

### By: Andrew Wairegi

## Description
To be able to identify whether emails from North America are spam or not. Using the Gaussian Naive
bayes classifier. This will allow us to create a spam/ham model, using a NLP model. It will give me an idea of how 
NLP models work. Especially in terms of the accuracy I should expect from them. It will also give me an idea of how spam filters work.

## Setup/Installation instructions
1. Create a folder on your computer
2. Set it up as a repository (using git init)
3. Clone this repository there (using git clone https://...)
4. Upload the notebooks to a google drive folder
5. Open it
6. Upload the data files for that collab in to the notebook (using the file upload section)
7. Execute the notebook

## Known Bugs
There are no known issues / bugs

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package
4. Seaborn - A Visualization package
5. Matplotlib - A Visualization package
6. Scikit learn - A Modelling package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/machine-learning/GaussianNB-Email_spam_vs_ham/-/blob/main/Email_spam_vs_ham_(gaussian_naive_bayes).ipynb
